#include "pic10c_movingdots.h"
#include "ui_pic10c_movingdots.h"


Pic10C_MovingDots::Pic10C_MovingDots(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::Pic10C_MovingDots)
{
    ui->setupUi(this);

    // The starting point of our dot
    this->x = 250;
    this->y = 250;
}

Pic10C_MovingDots::~Pic10C_MovingDots()
{
    delete ui;
}


// We'll keep it simple. A black dot somewhere inside the window.
void Pic10C_MovingDots::paintEvent( QPaintEvent *e){
    /**
        The QPainter class performs low-level painting on widgets
        and other paint devices.

        (See also: http://doc.qt.io/qt-4.8/qpainter.html#details )
    */
    QPainter painter(this);
    painter.setBrush( QBrush(Qt::black) );

    double width = 20.0;
    double height = 20.0;
    QRect rectangle( this->x, this->y, width, height );

    painter.drawEllipse(rectangle);

    return;
}


// Arrow keys move dot, others ignored.
void Pic10C_MovingDots::keyPressEvent( QKeyEvent *event ){
    switch ( event->key() ){
        case Qt::Key_Left :
            this->moveLeft();
            break;
        case Qt::Key_Right :
            this->moveRight();
            break;
        case Qt::Key_Up :
            this->moveUp();
            break;
        case Qt::Key_Down :
            this->moveDown();
            break;
        default:
            QWidget::keyPressEvent(event);
    }
    return;
}


// Common code shared by members

void Pic10C_MovingDots::handleEventsAndRepaint() {
    /** The QCoreApplication class provides an event loop for console
        Qt applications.

        processEvents() processes all pending events for the calling
        thread according to the specified flags until there are no more
        events to process.
    */
    QCoreApplication::processEvents();

    // The fields have been updated but the changes need to be displayed.
    this->repaint();
    return;
}


// Update x & y coordinates accordingly and repaint the widget.

void Pic10C_MovingDots::moveRight(){
    this->x += 20.0;
    this->handleEventsAndRepaint();
    return;
}

void Pic10C_MovingDots::moveLeft(){
    this->x -= 20.0;
    this->handleEventsAndRepaint();
    return;
}

void Pic10C_MovingDots::moveUp(){
    this->y -= 20.0;
    this->handleEventsAndRepaint();
    return;
}

void Pic10C_MovingDots::moveDown(){
    this->y += 20.0;
    this->handleEventsAndRepaint();
    return;
}

