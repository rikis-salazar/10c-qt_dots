#ifndef PIC10C_MOVINGDOTS_H
#define PIC10C_MOVINGDOTS_H


// A QMainWindow IS A QWidget (i.e., it inherits from QWidget)
#include <QMainWindow>
#include <QPainter>
#include <QKeyEvent>


namespace Ui {
class Pic10C_MovingDots;
}

class Pic10C_MovingDots : public QMainWindow
{
   /**
       Q: What is the purpose of the Q_OBJECT macro?

       A: From the Qt_Documentation (see http://doc.qt.io/qt-4.8/moc.html)

       " The Meta-Object Compiler, moc, is the program that handles
         Qt's C++ extensions.

         The moc tool reads a C++ header file. If it finds one or more
         class declarations that contain the Q_OBJECT macro, it produces
         a C++ source file containing the meta-object code for those
         classes. Among other things, meta-object code is required for
         the signals and slots mechanism, the run-time type information,
         and the dynamic property system.
   */
    Q_OBJECT

public:
    explicit Pic10C_MovingDots(QWidget *parent = 0);
    ~Pic10C_MovingDots();


    /**
       The first step we take to customize our QMainWindow is to overload
       the paintEvent() function inherited from QWidget.

       (See also http://doc.qt.io/qt-4.8/qpaintevent.html#details)

       " Paint events are sent to widgets that need to update themselves,
         for instance when part of a widget is exposed because a covering
         widget was moved.


       We'll keep it simple. A black dot somewhere inside the window.
    */
    void paintEvent( QPaintEvent *e );


    /**
       We can also overload the keyPressEvent() function from QWidget.

       (See also http://doc.qt.io/qt-4.8/qwidget.html#keyPressEvent)

       Arrow keys should move our dot. Anything else should be ignored.
    */
    void keyPressEvent( QKeyEvent *event );



// Customize our Widget with non-standard SLOTS that react to signals
public slots:
    /**
        Responds to an event (mouse click, key press, etc) by moving
        our dot to the right. (i.e., updating the x-coordinate.
    */

    void moveRight();
    /**
        Responds to an event (mouse click, key press, etc) by moving
        our dot to the left. (i.e., updating the x-coordinate.
    */
    void moveLeft();

    /**
        Responds to an event (mouse click, key press, etc) by moving
        our dot up. (i.e., updating the x-coordinate.
    */
    void moveUp();

    /**
        Responds to an event (mouse click, key press, etc) by moving
        our dot down. (i.e., updating the x-coordinate.
    */
    void moveDown();



private:
    Ui::Pic10C_MovingDots *ui;

    /**
        Process events & repaint widget
     */
    void handleEventsAndRepaint();


    /**
        The x and y coordinates of our dot.
     */
    double x;
    double y;

};

#endif // PIC10C_MOVINGDOTS_H
