#include "pic10c_movingdots.h"
#include <QApplication>
#include <QPushButton>
#include <QHBoxLayout>

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);

    // Notice: display_window is a LOCAL variable [Stack]
    Pic10C_MovingDots display_window;
    display_window.show();


    // Display control buttons in separate window with horizontal layout
    QPushButton *move_right = new QPushButton("Right");
    QObject::connect( move_right, SIGNAL(clicked()) ,
                      &display_window, SLOT(moveRight()) );

    QPushButton *move_left = new QPushButton("Left");
    QObject::connect( move_left, SIGNAL(clicked()) ,
                      &display_window, SLOT(moveLeft()) );

    QPushButton *move_up = new QPushButton("Up");
    QObject::connect( move_up, SIGNAL(clicked()) ,
                      &display_window, SLOT(moveUp()) );

    QPushButton *move_down = new QPushButton("Down");
    QObject::connect( move_down, SIGNAL(clicked()) ,
                      &display_window, SLOT(moveDown()) );


    // The Horizontal Layout containing the control buttons
    QHBoxLayout *layout = new QHBoxLayout;
    layout->addWidget(move_left);
    layout->addWidget(move_up);
    layout->addWidget(move_down);
    layout->addWidget(move_right);


    // The control window with the control buttons
    QWidget *control_window = new QWidget;
    control_window->setLayout(layout);
    control_window->setWindowTitle("Controls");
    control_window->show();


    return a.exec();
}
