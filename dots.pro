#-------------------------------------------------
#
# Project created by QtCreator 2016-10-13T11:43:25
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = dots
TEMPLATE = app


SOURCES += main.cpp\
        pic10c_movingdots.cpp

HEADERS  += pic10c_movingdots.h

FORMS    += pic10c_movingdots.ui
